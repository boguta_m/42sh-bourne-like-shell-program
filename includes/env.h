/*
** env.h for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 17:56:32 2014 camill_n
** Last update Fri May 23 14:35:49 2014 camill_n
*/

#ifndef ENV_H_
# define ENV_H_

int	unset_env(t_data *data, char **cmd);
int	del_env(char **env, char *val);
int	set_env(t_data *data, char **cmd);
int	export(t_data *data, char **cmd);
int	display_env(t_data *data, char **cmd);
char	*get_env(char **env, char *name);
char	*get_env_line(char **env, char *name);
char	**mysetenv(char **env, char *val);

#endif
