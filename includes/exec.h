/*
** exec.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May  1 12:06:55 2014 camill_n
** Last update Tue May 13 13:23:39 2014 camill_n
*/

#ifndef EXEC_H_
# define EXEC_H_

int	exec_node(t_data *data, t_node *node);

#endif
