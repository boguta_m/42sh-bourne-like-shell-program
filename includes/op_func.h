/*
** op_func.h for op_func in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 18 13:55:12 2014 camill_n
** Last update Fri May 23 17:25:29 2014 camill_n
*/

#ifndef OP_FUNC_H
# define OP_FUNC_H

int	pipe_func(t_data *data, t_node *node, int mode);
int	right_func(t_data *data, t_node *node, int mode);
int	left_func(t_data *data, t_node *node, int mode);
int	dleft_func(t_data *data, t_node *node, int mode);
int	pipe_action(int *pipefd, int *save, int fd);
int	logic_func(t_data *data, t_node *node, int mode);
int	is_redir(char *str);

#endif
