/*
** termios.h for termios.h in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Jan  8 19:34:23 2014 Nicolas Camilli
** Last update Sun May 25 23:39:01 2014 Nicolas Camilli
*/

#ifndef TIOS_H_
# define TIOS_H_

# include <termios.h>
# include <sys/ioctl.h>
# include <curses.h>
# include <term.h>

# define CAP data->cap

typedef struct termios t_termios;
typedef struct s_term t_term;
typedef struct winsize t_winsize;
typedef struct s_data t_data;

typedef struct	s_cap
{
  char		*us;
  char		*ue;
  char		*cl;
  char		*so;
  char		*se;
  char		*ti;
  char		*te;
  char		*cm;
  char		*kl;
  char		*le;
  char		*nd;
  char		*ec;
  char		*dc;
}		t_cap;

int	init_tios(t_data *data, t_term *te, t_termios *ts, t_termios *ts_save);
int	set_raw(struct termios *termios);
int	set_termi(struct termios *termios);
int	get_termi(struct termios *termios);

#endif
