/*
** alias.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri May 16 12:42:02 2014 camill_n
** Last update Sun May 25 11:55:55 2014 camill_n
*/

#ifndef ALIAS_H_
# define ALIAS_H_

typedef struct		s_alias
{
  char			*old;
  char			*new;
  struct s_alias	*next;
}			t_alias;

void	add_alias(t_data *data, char *old, char *new);
void	del_alias(t_data *data, t_alias *alias);
int	set_alias(t_data *data, char **cmd);
int	unset_alias(t_data *data, char **cmd);
int	check_alias(t_data *data, char ***cmd);
int	format_tab(t_data *data, char ***new, char ***cmd);

#endif
