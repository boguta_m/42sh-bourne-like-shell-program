/*
** bind.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 17:04:10 2014 camill_n
** Last update Sun May 25 21:32:59 2014 Antonin Bouscarel
*/

#ifndef BIND_H_
# define BIND_H_

typedef struct		s_bind
{
  char			save[5];
  int			size;
  int			(*exec)(t_data *);
  struct s_bind		*next;
}			t_bind;

int	ctrl_a(t_data *data);
int	ctrl_e(t_data *data);
int	ctrl_u(t_data *data);
int	ctrl_k(t_data *data);
int	ctrl_w(t_data *data);
int	ctrl_t(t_data *data);
int	ctrl_l(t_data *data);
int	ctrl_y(t_data *data);
int	alt_b(t_data *data);
int	alt_f(t_data *data);
int	alt_c(t_data *data);
int	alt_d(t_data *data);
int	keydel1(t_data *data);
int	keydel2(t_data *data);
int	keyup(t_data *data);
int	keydown(t_data *data);
int	keyleft(t_data *data);
int	keyright(t_data *data);
void	init_bind(t_data *data);
int	verif_bind(t_data *data, char *save);
int	keysupr(t_data *data);
int	verif_utils(char *s1, char *s2, int size);

#endif
