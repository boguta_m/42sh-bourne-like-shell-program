/*
** builtins.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Mar 14 19:42:34 2014 camill_n
** Last update Tue Jun 10 22:42:28 2014 Maxime Boguta
*/

#ifndef BUILTINS_H_
# define BUILTINS_H_

typedef struct		s_built
{
  char			*cmd;
  int			(*exec)(t_data *data, char **);
  struct s_built	*next;
}			t_built;

void	add_built(t_data *data, char *cmd, int (*exec)(t_data *data, char **));
int	find_builtins_exec(t_data *data, char **argv);
int	builtins_exec(t_data *data, char **argv);
void	init_built(t_data *data);
int	my_chdir(t_data *data, char **cmd);
int	decal_str(char *, int);

#endif
