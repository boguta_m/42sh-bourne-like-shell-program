/*
** list.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 14:34:25 2014 camill_n
** Last update Fri May 16 05:59:11 2014 camill_n
*/

#include "global.h"

int	init_list(t_cmd *cmd)
{
  cmd->nb_elem = 0;
  cmd->start = NULL;
  cmd->end = NULL;
  add_cmd(cmd);
  cmd->current = cmd->end;
  return (0);
}

int	add_cmd(t_cmd *cmd)
{
  t_req	*req;

  req = x_malloc(sizeof(t_req), "req");
  req->pos = 0;
  req->req[0] = 0;
  bzero(req->req, 5000);
  if (cmd->nb_elem == 0)
    {
      cmd->start = req;
      cmd->end = req;
      req->prev = req;
      req->next = req;
    }
  else
    {
      cmd->end->next = req;
      req->prev = cmd->end;
      cmd->end = req;
      req->next = cmd->start;
    }
  cmd->current = cmd->end;
  ++cmd->nb_elem;
  return (0);
}

int	display_list(t_data *data, char **cmd)
{
  t_req	*tmp;
  int	i;

  tmp = data->cmd->start;
  i = 1;
  if (cmd[1] != NULL && !strcmp(cmd[1], "-c"))
    {
      data->cmd->start = CUR_CMD;
      data->cmd->nb_elem = 1;
    }
  while (i < data->cmd->nb_elem)
    {
      printf("  %03d  %s\n", i, tmp->req);
      tmp = tmp->next;
      ++i;
    }
  return (0);
}
