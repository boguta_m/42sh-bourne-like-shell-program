/*
** alias.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri May 16 12:41:31 2014 camill_n
** Last update Tue Jun 10 22:42:47 2014 Maxime Boguta
*/

#include "global.h"

void		add_alias(t_data *data, char *old, char *new)
{
  t_alias	*tmp;
  t_alias	*new_alias;

  new_alias = x_malloc(sizeof(t_alias), "alias");
  new_alias->old = strdup(old);
  new_alias->new = strdup(new);
  new_alias->next = NULL;
  if (data->alias == NULL)
    data->alias = new_alias;
  else
    {
      tmp = data->alias;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_alias;
    }
}

int		set_alias(t_data *data, char **cmd)
{
  t_alias	*tmp;

  if (cmd[1] != NULL && cmd[2] != NULL)
    add_alias(data, cmd[1], cmd[2]);
  else
    {
      tmp = data->alias;
      while (tmp)
	{
	  printf("alias %s='%s'\n", tmp->old, tmp->new);
	  tmp = tmp->next;
	}
    }
  return (0);
}

void		del_alias(t_data *data, t_alias *alias)
{
  t_alias	*tmp;

  if (data->alias != NULL && data->alias->next == NULL)
    data->alias = NULL;
  else if (data->alias == alias)
    data->alias = alias->next;
  else
    {
      tmp = data->alias;
      while (tmp->next != alias)
	tmp = tmp->next;
      if (alias->next != NULL)
	tmp->next = alias->next;
      else
	tmp->next = NULL;
    }
  free(alias);
}

int		unset_alias(t_data *data, char **cmd)
{
  t_alias	*alias;

  if (cmd[1] == NULL)
    return (-1);
  alias = data->alias;
  while (alias)
    {
      if (!strcmp(alias->old, cmd[1]))
	{
	  del_alias(data, alias);
	  return (0);
	}
      alias = alias->next;
    }
  return (0);
}

int		check_alias(t_data *data, char ***cmd)
{
  t_alias	*alias;
  char		**new_tab;

  alias = data->alias;
  if (cmd[0][0][0] == '\\')
    return (decal_str(cmd[0][0], -1));
  while (alias)
    {
      if (!strcmp(cmd[0][0], alias->old))
	{
	  free(cmd[0][0]);
	  cmd[0][0] = strdup(alias->new);
	  new_tab = my_wordtab(cmd[0][0], ' ');
	  if (new_tab[1] == NULL)
	    return (1);
	  else
	    format_tab(data, &new_tab, cmd);
	  return (1);
	}
      alias = alias->next;
    }
  return (0);
}
