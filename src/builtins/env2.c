/*
** env2.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed May 21 12:33:39 2014 camill_n
** Last update Wed Jun 11 12:44:34 2014 Maxime Boguta
*/

#include "global.h"

int	set_env(t_data *data, char **cmd)
{
  char	*new;
  int	size;

  if (cmd[1] == NULL || cmd[2] == NULL)
    return (-1);
  del_env(data->env, cmd[1]);
  size = (strlen(cmd[1]) + strlen(cmd[2]) + 2) * sizeof(char);
  new = x_malloc(size, "new");
  bzero(new, size);
  strcat(new, cmd[1]);
  strcat(new, "=");
  strcat(new, (cmd[2]));
  data->env = mysetenv(data->env, new);
  return (0);
}

int	unset_env(t_data *data, char **cmd)
{
  del_env(data->env, cmd[1]);
  return (0);
}

int	export(t_data *data, char **cmd)
{
  int	i;

  i = 0;
  if (cmd[1] != NULL)
    {
      while (cmd[1][i])
	{
	  if (cmd[1][i] == '=')
	    {
	      cmd[1][i] = 0;
	      del_env(data->env, cmd[1]);
	      cmd[1][i] = '=';
	    }
	  ++i;
	}
      data->env = mysetenv(data->env, cmd[1]);
    }
  return (0);
}

int	display_env(t_data *data, char **cmd)
{
  my_show_wordtab(data->env);
  return (0);
}
