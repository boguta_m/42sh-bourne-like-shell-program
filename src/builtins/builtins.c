/*
** builtins.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat Mar 15 00:01:08 2014 camill_n
** Last update Wed Jun 11 01:25:57 2014 Maxime Boguta
*/

#include <errno.h>
#include "global.h"

void		add_built
(t_data *data, char *cmd, int (*exec)(t_data *data, char **cmd))
{
  t_built	*tmp;
  t_built	*new_built;

  new_built = x_malloc(sizeof(t_built), "built");
  new_built->cmd = strdup(cmd);
  new_built->exec = exec;
  new_built->next = NULL;
  if (data->built == NULL)
    data->built = new_built;
  else
    {
      tmp = data->built;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_built;
    }
}

int		find_builtins_exec(t_data *data, char **cmd)
{
  t_built	*built;

  built = data->built;
  while (built)
    {
      if (strcmp(cmd[0], built->cmd) == 0)
	return (1);
      built = built->next;
    }
  return (0);
}

int		builtins_exec(t_data *data, char **cmd)
{
  t_built	*built;

  built = data->built;
  while (built)
    {
      if (strcmp(cmd[0], built->cmd) == 0)
	return (built->exec(data, cmd));
      built = built->next;
    }
  my_free_wordtab(cmd);
  return (0);
}

int		my_exit(t_data *data, char **cmd)
{
  data->run = 0;
  if (cmd[1])
    data->lastexit = atoi(cmd[1]);
  return (data->lastexit);
}

void		init_built(t_data *data)
{
  add_built(data, "env", display_env);
  add_built(data, "history", display_list);
  add_built(data, "cd", my_chdir);
  add_built(data, "setenv", set_env);
  add_built(data, "export", export);
  add_built(data, "unsetenv", unset_env);
  add_built(data, "setalias", set_alias);
  add_built(data, "unsetalias", unset_alias);
  add_built(data, "echo", my_echo);
  add_built(data, "exit", my_exit);
}
