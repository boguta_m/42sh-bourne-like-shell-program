/*
** env.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 17:51:25 2014 camill_n
** Last update Wed Jun 11 12:44:32 2014 Maxime Boguta
*/

#include "global.h"

char	**mysetenv(char **env, char *val)
{
  int	i;
  int	size;
  char	**tabl;
  char	*verif;

  if (env == NULL || val == NULL || strlen(val) < 1)
    return (env);
  verif = get_env(env, val);
  if (verif != NULL)
    {
      verif != NULL ? free(verif) : 0;
      return (env);
    }
  size = get_sizetab(env) + 2;
  tabl = x_malloc(size * sizeof(char *), "env");
  i = 0;
  while (env[i] != NULL)
    {
      tabl[i] = strdup(env[i]);
      ++i;
    }
  tabl[i] = strdup(val);
  tabl[i + 1] = NULL;
  my_free_wordtab(env);
  return (tabl);
}

char	*get_env(char **env, char *name)
{
  int	i;

  i = 0;
  if (env == NULL || name == NULL || strlen(name) < 1)
    return (NULL);
  while (env[i] != NULL)
    {
      if (strncmp(env[i], name, strlen(name)) == 0)
	return (strdup(env[i] + strlen(name) + 1));
      ++i;
    }
  return (NULL);
}

int	del_env(char **env, char *name)
{
  int	i;
  int	check;

  i = 0;
  check = 0;
  if (env == NULL || name == NULL || strlen(name) < 1)
    return (-1);
  while (env[i] != NULL)
    {
      if (strncmp(env[i], name, strlen(name)) == 0)
	{
	  check = 1;
	  while (env[i + 1] != NULL)
	    {
	      env[i] = env[i + 1];
	      ++i;
	    }
	}
      ++i;
    }
  check == 1 ? env[i - 1] = NULL : 0;
  check == 1 ? free(env[i]) : 0;
  return (0);
}

char	*get_env_line(char **env, char *name)
{
  int	i;

  i = 0;
  if (env == NULL || name == NULL || strlen(name) < 1)
    return (NULL);
  while (env[i] != NULL)
    {
      if (strncmp(env[i], name, strlen(name)) == 0)
	return (env[i]);
      ++i;
    }
  return (0);
}

int	my_echo(t_data *data, char **cmd)
{
  char	*p;

  p = NULL;
  if (cmd[1] && cmd[1][0] == '$' && cmd[1][1] != ' ' && cmd[1][1] != 0)
    {
      if (cmd[1][1] == '?' && (cmd[1][2] == ' ' || cmd[1][2] == 0))
	{
	  printf("%d\n", data->lastexit);
	  return (0);
	}
      p = get_env(data->env, &cmd[1][1]);
      if (p)
	{
	  printf("%s\n", p);
	  free(p);
	}
      else
	printf("\n");
      return (0);
    }
  (cmd[1]) ? printf("%s", cmd[1]) : 0;
  printf("\n");
  return (0);
}
