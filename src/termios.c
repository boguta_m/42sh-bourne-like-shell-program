/*
** termios.c for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Jan  8 19:31:09 2014 Nicolas Camilli
** Last update Sun May 25 23:19:00 2014 Nicolas Camilli
*/

#include "global.h"

int	init_tios(t_data *data, t_term *term, t_termios *ti, t_termios *ti_s)
{
  get_termi(ti);
  get_termi(ti_s);
  set_raw(ti);
  set_termi(ti);
  term->tios = ti;
  term->clip = NULL;
  term->tios_save = ti_s;
  data->term = term;
  return (0);
}

int	set_raw(t_termios *termios)
{
  termios->c_lflag &= ~ICANON;
  termios->c_lflag &= ~ECHO;
  termios->c_cc[VMIN] = 1;
  termios->c_cc[VTIME] = 0;
  set_termi(termios);
  return (0);
}

int	get_termi(t_termios *termios)
{
  int	ret;

  ret = tcgetattr(0, termios);
  ret == -1 ? displayError(TIOS_GET_ERROR, NULL) : 0;
  return (0);
}

int	set_termi(t_termios *termios)
{
  int	ret;

  ret = tcsetattr(0, 0, termios);
  ret == -1 ? displayError(TIOS_SET_ERROR, NULL) : 0;
  return (0);
}
