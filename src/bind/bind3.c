/*
** bind3.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri May 16 02:46:22 2014 camill_n
** Last update Sun May 25 22:26:22 2014 BOURSET Axel
*/

#include "global.h"

int	keydel1(t_data *data)
{
  if (strlen(CUR_CMD->req) > 0 && CUR_CMD->pos > 0)
    del_char(CUR_CMD, CUR_CMD->pos, 0);
  return (0);
}

int	keydel2(t_data *data)
{
  if (strlen(CUR_CMD->req) > 0)
    del_char(CUR_CMD, CUR_CMD->pos, 1);
  return (0);
}
