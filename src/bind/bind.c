/*
** bind.c for 42sh in /home/bourse_a/Documents/B2_Systeme_Unix/psu_2013_42sh/src
**
** Made by BOURSET Axel
** Login   <bourse_a@epitech.net>
**
** Started on  Tue May 13 13:09:10 2014 BOURSET Axel
** Last update Sun May 25 23:35:39 2014 Nicolas Camilli
*/

#include "global.h"

void		add_bind(t_data *data, char *save, int s, int (*exec)(t_data *))
{
  t_bind	*tmp;
  t_bind	*new_bind;

  new_bind = x_malloc(sizeof(t_bind), "ne");
  bzero(new_bind->save, sizeof(new_bind->save));
  s > 0 ? memcpy(new_bind->save, save, s) : 0;
  new_bind->exec = exec;
  new_bind->size = s;
  s == -1 ? memcpy(new_bind->save, "\e[3~", 4) : 0;
  s == -1 ? new_bind->size = 4 : 0;
  new_bind->next = NULL;
  if (data->bind == NULL)
    data->bind = new_bind;
  else
    {
      tmp = data->bind;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_bind;
    }
}

int		verif_utils(char *s1, char *s2, int size)
{
  int		ret;
  int		i;

  i = 0;
  ret = 4 - size;
  s2 += ret;
  while (i < size)
    {
      if (s1[i] != s2[i])
	return (0);
      ++i;
    }
  return (1);
}

char		*set_save(char *dst, char c1, char c2, char c3)
{
  dst[0] = c1;
  dst[1] = c2;
  dst[2] = c3;
  return (dst);
}

void		init_bind_2(t_data *data)
{
  add_bind(data, set_save(data->buff, 1, 0, 0), 1, ctrl_a);
  add_bind(data, set_save(data->buff, 21, 0, 0), 1, ctrl_u);
  add_bind(data, set_save(data->buff, 11, 0, 0), 1, ctrl_k);
  add_bind(data, set_save(data->buff, 23, 0, 0), 1, ctrl_w);
  add_bind(data, set_save(data->buff, 25, 0, 0), 1, ctrl_y);
  add_bind(data, set_save(data->buff, 5, 0, 0), 1, ctrl_e);
  add_bind(data, set_save(data->buff, 20, 0, 0), 1, ctrl_t);
  add_bind(data, set_save(data->buff, 12, 0, 0), 1, ctrl_l);
  add_bind(data, set_save(data->buff, 8, 0, 0), 1, keydel1);
  add_bind(data, set_save(data->buff, 2, 0, 0), 1, keyleft);
  add_bind(data, set_save(data->buff, 16, 0, 0), 1, keyup);
  add_bind(data, set_save(data->buff, 6, 0, 0), 1, keydown);
  add_bind(data, set_save(data->buff, 127, 0, 0), 1, keydel1);
  add_bind(data, set_save(data->buff, 27, 91, 68), 3, keyleft);
  add_bind(data, set_save(data->buff, 27, 91, 65), 3, keyup);
  add_bind(data, set_save(data->buff, 27, 91, 66), 3, keydown);
  add_bind(data, set_save(data->buff, 27, 91, 67), 3, keyright);
  add_bind(data, set_save(data->buff, 27, 'b', 0), 2, alt_b);
}

void		init_bind(t_data *data)
{
  data->bind = NULL;
  init_bind_2(data);
  add_bind(data, "\ef", 2, alt_f);
  add_bind(data, "\ec", 2, alt_c);
  add_bind(data, "\ed", 2, alt_d);
  add_bind(data, "\ei", 2, alt_f);
  add_bind(data, set_save(data->buff, 9, 0, 0), 1, NULL);
  add_bind(data, set_save(data->buff, 27, '[', '3'), -1, keydel2);
}
