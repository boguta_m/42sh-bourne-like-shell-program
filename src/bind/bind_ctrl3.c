/*
** bind_ctrl3.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 20 08:51:02 2014 camill_n
** Last update Sun May 25 23:17:09 2014 Nicolas Camilli
*/

#include "global.h"

int	ctrl_y(t_data *data)
{
  int	size;
  int	i;

  i = 0;
  if (TERM->clip != NULL)
    {
      size = strlen(TERM->clip);
      while (i != size)
	set_char(data, TERM->clip[i++]);
    }
  return (0);
}

int	ctrl_w(t_data *data)
{
  int	i;
  int	check;

  i = CUR_CMD->pos;
  check = 0;
  if (CUR_CMD->pos == 0)
    return (0);
  while (check != 2 && i != 0)
    {
      if (check == 0)
	if (CUR_CMD->req[i - 1] >= 33 && CUR_CMD->req[i - 1] <= 126)
	  ++check;
      if (check == 1)
	if (CUR_CMD->req[i - 1] == ' ')
	  ++check;
      check == 2 ? 0 : i--;
    }
  set_clip(data, i, CUR_CMD->pos);
  while (i != CUR_CMD->pos)
    del_char(CUR_CMD, CUR_CMD->pos, 0);
  return (0);
}

int	alt_d(t_data *data)
{
  int	i;
  int	check;
  int	size;
  int	save;

  i = CUR_CMD->pos;
  check = 0;
  size = strlen(CUR_CMD->req);
  while (check != 2 && i != size)
    {
      if (check == 0)
	if (CUR_CMD->req[i] >= 33 && CUR_CMD->req[i] <= 126)
	  check++;
      if (check == 1)
	if (CUR_CMD->req[i] == ' ')
	  check++;
      check == 2 ? 0 : i++;
    }
  save = CUR_CMD->pos;
  CUR_CMD->pos = i;
  while (i != save)
    del_char(CUR_CMD, i--, 0);
  return (0);
}
