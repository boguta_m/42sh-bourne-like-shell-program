/*
** clipboard.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh/src/bind
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 25 10:52:08 2014 camill_n
** Last update Wed Jun 11 00:41:53 2014 Maxime Boguta
*/

#include "global.h"

void	set_clip(t_data *data, int i, int j)
{
  if (TERM->clip != NULL)
    {
      free(TERM->clip);
      TERM->clip = NULL;
    }
  TERM->clip = x_malloc((j - i + 2) * sizeof(char), "Clipboard");
  bzero(TERM->clip, sizeof(*TERM->clip) + 1);
  memcpy(TERM->clip, CUR_CMD->req + i, j - i + 1);
}
