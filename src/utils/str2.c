/*
** str2.c for str2 in /home/boguta_m/rendu/42sh/src/utils
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Jun 10 23:17:56 2014 Maxime Boguta
** Last update Wed Jun 11 00:26:38 2014 Maxime Boguta
*/

#include <string.h>
#include <stdlib.h>
#include "alloc.h"

char		*my_strcut(char *tocut, char sep)
{
  int		i;
  char		*ret;

  ret = strdup(tocut);
  i = 0;
  while (tocut[i] != sep && tocut[i] != 0)
    i++;
  ret[i] = 0;
  return (ret);
}

void		decal_str_up(char *s, int amt)
{
  int		i;
  char		*w;

  i = 0;
  w = strdup(s);
  while (w[i])
    {
      s[i + amt] = w[i];
      i++;
    }
  s[i + amt] = 0;
}

char		*insert_str(char *to, char *ins, int pos)
{
  int		i;
  char		*ret;

  i = 0;
  ret = x_malloc(strlen(to + 1) + strlen(ins) + 3, "insert");
  strcpy(ret, to);
  while (ret[pos] != 0 && ret[pos] != ' ' && ins[i])
    {
      ret[pos] = ins[i];
      pos++;
      i++;
    }
  while (ins[i])
    {
      decal_str_up(&ret[pos], 1);
      ret[pos] = ins[i];
      pos++;
      i++;
    }
  return (ret);
}
