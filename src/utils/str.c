/*
** str.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 13 11:27:02 2014 camill_n
** Last update Tue Jun 10 22:45:23 2014 Maxime Boguta
*/

#include "global.h"

int	my_putchar(int nb)
{
  write(1, &nb, 1);
  return (0);
}

char	*my_strcat(char *s1, char *s2)
{
  int	size;
  char	*new;
  int	i;
  int	k;

  i = 0;
  size = strlen(s1) + strlen(s2) + 1;
  new = x_malloc(size * sizeof(char), "new");
  bzero(new, size);
  while (s1[i])
    {
      new[i] = s1[i];
      ++i;
    }
  k = 0;
  while (s2[k])
    {
      new[i] = s1[k];
      ++i;
      ++k;
    }
  return (new);
}

void	my_strcatt(char *s1, char *s2)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (s1 && s1[i])
    i++;
  while (s2 && s2[j])
    {
      s1[i] = s2[j];
      j++;
      i++;
    }
  s1[i] = 0;
}

int	get_state(t_wd *wtab, char **in)
{
  int	j;
  int	i;
  int	state;

  state = 0;
  i = -1;
  while (in[++i])
    {
      j = -1;
      while (in[i][++j])
	if (in[i][j] == '"')
	  state = !state;
    }
  return (state);
}

int	decal_str(char *s, int si)
{
  int	i;
  int	j;

  i = 0;
  j = -si;
  while (s[j])
    {
      s[i] = s[j];
      i++;
      j++;
    }
  s[j + si] = 0;
  return (si);
}
