/*
** utils.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 16:10:30 2014 camill_n
** Last update Sun May 25 23:40:36 2014 Nicolas Camilli
*/

#include "global.h"

int		set_tab_char(t_data *data)
{
  int		size;

  size = strlen(CUR_CMD->req);
  while (CUR_CMD->req[CUR_CMD->pos] != ' ' && CUR_CMD->pos != size)
    CUR_CMD->pos++;
  return (0);
}

int		del_char(t_req *req, int pos, int mode)
{
  int		size;
  char		*buff;

  buff = req->req;
  size = strlen(buff);
  if (pos == size)
    {
      if (mode == 0)
	{
	  buff[size - 1] = '\0';
	  --(req->pos);
	}
      return (0);
    }
  mode == 0 ? --pos : 0;
  while (pos < size + 1)
    {
      buff[pos] = buff[pos + 1];
      ++pos;
    }
  mode == 0 ? --(req->pos) : 0;
  return (0);
}

int		replace(t_data *data, char *pattern, char *sol)
{
  int		i;
  int		size;
  int		j;

  i = 0;
  size = strlen(sol) - strlen(pattern) + 1;
  j = strlen(pattern) - 1;
  if (CUR_CMD->req[CUR_CMD->pos] == ' ')
    set_tab_char(data);
  while (i < size)
    {
      set_char(data, sol[j]);
      ++j;
      ++i;
    }
  return (0);
}

int		replace_comp(t_data *data, char *pattern, char **sol)
{
  int		i;
  int		check;
  int		cpt;

  i = 0;
  check = 0;
  cpt = strlen(pattern) - 1;
  while (!check)
    {
      i = 0;
      while (!check && sol[i] != NULL)
	{
	  if (strncmp(sol[0], sol[i], cpt) != 0)
	    check = 1;
	  ++i;
	}
      !check ? ++cpt : 0;
    }
  if (cpt > strlen(pattern))
    {
      sol[0][cpt - 1] = '\0';
      replace(data, pattern, sol[0]);
    }
  return (0);
}

int		search(t_data *data, char *pattern, int mode, char *save)
{
  glob_t	globa;
  int		i;
  char		*tmp;

  i = 0;
  if (strlen(pattern) == 0)
    return (0);
  tmp = strcat(pattern, "*");
  glob(tmp, GLOB_MARK, NULL, &globa);
  if (mode == 0)
    {
      if (globa.gl_pathc == 1)
	replace(data, pattern, globa.gl_pathv[0]);
      else if (globa.gl_pathc > 0)
	replace_comp(data, pattern, globa.gl_pathv);
      globfree(&globa);
      return (-1);
    }
  (globa.gl_pathc > 0) ? putchar('\n') : 0;
  while (i < globa.gl_pathc && mode == 1)
    printf("%s\n", globa.gl_pathv[i++]);
  globfree(&globa);
  return (0);
}
