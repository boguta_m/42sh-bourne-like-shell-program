/*
** wordtab.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Mar 14 19:32:36 2014 camill_n
** Last update Wed Jun 11 01:46:00 2014 Maxime Boguta
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "alloc.h"

int	get_nb_word(char *str, char c)
{
  int	i;
  int	cpt;

  cpt = 1;
  i = 0;
  while (str[i] != '\0')
    {
      (str[i] == ' ' && str[i + 1] != ' ') ? ++cpt : 0;
      (str[i] == c && str[i + 1] != c) ? ++cpt : 0;
      ++i;
    }
  return (cpt);
}

void	my_free_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab && tab[i] != NULL)
    {
      free(tab[i]);
      ++i;
    }
  if (tab)
    {
      free(tab[i]);
      free(tab);
    }
}

void	my_show_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    {
      printf("%s\n", tab[i]);
      ++i;
    }
}

int	get_sizetab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    ++i;
  return (i);
}

char	**duptab(char **tab)
{
  int	i;
  char	**new_tab;
  int	size;

  size = get_sizetab(tab) + 1;
  new_tab = x_malloc(size * sizeof(char *), "tab");
  if (new_tab == NULL)
    return (NULL);
  i = 0;
  while (tab[i] != NULL)
    {
      new_tab[i] = strdup(tab[i]);
      ++i;
    }
  new_tab[i] = NULL;
  return (new_tab);
}
