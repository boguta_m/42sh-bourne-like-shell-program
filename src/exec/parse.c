/*
** tree.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 12:11:40 2014 camill_n
** Last update Wed Jun 11 01:39:16 2014 Maxime Boguta
*/

#include "global.h"

int		check_end(char *req, int pos, t_exec *exec, int type_d)
{
  int		ret;
  int		type;
  int		tmp;
  int		check;

  check = 0;
  type = -1;
  tmp = pos;
  while (exec)
    {
      exec->op == type_d ? pos += strlen(exec->sym): 0;
      exec = exec->next;
    }
  ret = get_op_pos(req + pos, &type, exec);
  if (ret != -1)
    return (ret + 1);
  while (req[pos] != 0 && !check)
    {
      if (ret != -1 && pos >= ret)
	++check;
      if (req[pos] != ' ' && req[pos] != '\t')
	return (pos - tmp);
      ++pos;
    }
  return (0);
}

int		sub_req(t_data *data, t_node *node)
{
  int		pos;
  int		type;
  char		*left;

  type = -1;
  pos = get_op_pos(node->req, &type, data->exec);
  if (pos == -1)
    return (0);
  left = x_malloc((pos + 1) * sizeof(char), "left");
  memcpy(left, node->req, pos);
  left[pos] = 0;
  node->op = type;
  node->left = set_node(left, data->env);
  sub_req(data, node->left);
  if (check_end(REQ, pos, data->exec, type) > 0)
    {
      if (type == 2 || type == 3 || type == 6 || type == 7)
	node->right = set_node(REQ + pos + 2, data->env);
      else
	node->right = set_node(REQ + pos + 1, data->env);
      sub_req(data, node->right);
    }
  REQ != NULL ? free(REQ) : 0;
  free(left);
  return (0);
}

int		check_syntax(t_node *node, int *exec, t_exec *op)
{
  int		i;

  if (node == NULL || *exec != 0)
    return (0);
  if (node->op == 0)
    {
      i = 0;
      while (REQ[i] != 0 && (REQ[i] == ' ' || REQ[i] == '\t'))
	++i;
      i == strlen(REQ) ? displayError(SYNTAX_ERROR, "empty line") : 0;
      i == strlen(REQ) ? ++(*exec) : 0;
    }
  if (node->op != 0)
    {
      if (NLEFT == NULL || (NRIGHT == NULL && node->op != 1))
	{
	  displayError(SYNTAX_ERROR, ";");
	  ++(*exec);
	}
    }
  check_syntax(node->left, exec, op);
  check_syntax(node->right, exec, op);
  return (0);
}

void		modif_line(char *str)
{
  char		**tabl;
  int		i;

  i = 3;
  tabl = my_wordtab(str, ' ');
  if (is_redir(tabl[0]) && tabl[1] != NULL && tabl[2] != NULL)
    {
      bzero(str, strlen(str));
      strcat(str, tabl[2]);
      strcat(str, " ");
      strcat(str, tabl[0]);
      strcat(str, " ");
      strcat(str, tabl[1]);
      while (tabl[i])
      	{
	  strcat(str, " ");
      	  strcat(str, tabl[i]);
      	  ++i;
      	}
    }
  my_free_wordtab(tabl);
}

int		parse(t_data *data, char *l)
{
  t_node	*node;
  int		exec;

  exec = -1;
  while (CUR_CMD->req && CUR_CMD->req[++exec] == ' '
	 && CUR_CMD->req[exec] != 0);
  if (CUR_CMD->req && CUR_CMD->req[exec] == 0 && CUR_CMD->req[0] != 0)
    return (0);
  exec = 0;
  if (l != NULL)
    node = set_node(l, data->env);
  else
    node = set_node(CUR_CMD->req, data->env);
  modif_line(node->req);
  sub_req(data, node);
  check_syntax(node, &exec, data->exec);
  exec == 0 ? set_termi(data->term->tios_save) : 0;
  exec == 0 ? exec_node(data, node) : 0;
  exec == 0 ? set_termi(data->term->tios) : 0;
  free_tree(node);
  return (0);
}
