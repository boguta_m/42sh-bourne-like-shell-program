/*
** op_func2.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May 15 12:06:27 2014 camill_n
** Last update Tue Jun 10 19:42:14 2014 Maxime Boguta
*/

#include <unistd.h>
#include <sys/wait.h>
#include "global.h"

int	read_char(t_data *data, t_node *node)
{
  char	buff[1024];
  int	ret2;
  int	check;
  char	**tabl;

  check = 0;
  tabl = my_wordtab(NRIGHT->req, ' ');
  while (check == 0)
    {
      write(0, "> ", 2);
      if (!check && (ret2 = read(0, buff, 1024)) > 0)
	{
	  buff[ret2 - 1] = '\0';
	  if (strcmp(buff, tabl[0]) == 0)
	    check = 1;
	  else
	    {
	      write(1, buff, strlen(buff));
	      write(1, "\n", 1);
	    }
	}
      check = (ret2 == 0 ? 1 : check);
    }
  my_free_wordtab(tabl);
  return (0);
}

int	verif_dir(t_node *node)
{
  if (RIGHT_T != 0 || LEFT_T != 0)
    {
      printf("Ambiguous output redirect.\n");
      return (-1);
    }
  return (0);
}

int	dleft_func(t_data *data, t_node *node, int mode)
{
  int	pipefd[2];
  int	ret;
  int	pid;
  int	save;

  if (verif_dir(node) == -1)
    return (-1);
  ret = pipe(pipefd);
  if (ret == -1)
    return (-1);
  if ((pid = fork()) == 0)
    {
      pipe_action(pipefd, &save, 0);
      read_char(data, node);
      dup2(save, 1);
      exit(0);
    }
  else
    {
      wait(NULL);
      pipe_action(pipefd, &save, 1);
      exec_node(data, NLEFT);
      dup2(save, 0);
    }
  return (0);
}

int	logic_func(t_data *data, t_node *node, int mode)
{
  int	ret;
  int	ret2;

  ret2 = -1;
  ret = exec_node(data, NLEFT);
  if (node->op == DAND && ret != -1)
    return (exec_node(data, NRIGHT));
  if (node->op == DOR)
    {
      if (ret == -1)
	ret2 = exec_node(data, NRIGHT);
      if (ret2 == -1 && ret == -1)
	return (-1);
      return (0);
    }
  return (0);
}
